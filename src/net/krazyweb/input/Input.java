package net.krazyweb.input;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class Input {
	
	private static boolean leftButtonDown = false;
	
	public static enum MOUSE {
		LEFT, RIGHT, MIDDLE
	}
	
	public static boolean isKeyDown(int key) {
		return Keyboard.isKeyDown(key);
	}
	
	public static boolean isKeyPressed(int key) {
		while(Keyboard.next()) {
			return Keyboard.isKeyDown(key);
		}
		return false;
	}
	
	public static int getMouseX() {
		return Mouse.getX();
	}
	
	public static int getMouseY() {
		return Mouse.getY();
	}
	
	public static boolean isMouseClicked(MOUSE mouse) {
		switch (mouse) {
		
			case LEFT:
				
				if (leftButtonDown && !Mouse.isButtonDown(0)){
					leftButtonDown = false;
				}
				
				if (!leftButtonDown && Mouse.isButtonDown(0)) {
					leftButtonDown = true;
					return true;
				} else if (leftButtonDown && Mouse.isButtonDown(0)) {
					return false;
				} else {
					return false;
				}
				
			default:
				break;
				
		}
		return false;
	}
	
	public static boolean isMouseDown(MOUSE mouse) {
		switch (mouse) {
			case LEFT:
				return Mouse.isButtonDown(0);
			default:
				break;
		}
		return false;
	}
	
}
