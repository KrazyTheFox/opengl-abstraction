package net.krazyweb.opengl;

import java.util.ArrayList;


public class OpenGLMesh {
	
	protected int VBOVertices;
	protected int VBOIndices;
	protected int VBOColors;
	protected int VBOTextures;
	
	protected int VAO;
	
	protected ArrayList<Float> vertexList;
	protected ArrayList<Integer> indexList;
	protected ArrayList<Float> colorList;
	protected ArrayList<Float> textureList;
	
	protected int numberOfIndices = 0;
	
}