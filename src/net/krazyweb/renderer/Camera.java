package net.krazyweb.renderer;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public interface Camera {

	public abstract Matrix4f viewMatrix();
	public abstract Matrix4f orthoMatrix();

	public abstract void offsetPosition(Vector3f displacement);
	public abstract void offsetOrientation(float upAngle, float rightAngle);

	public abstract void setPosition(Vector3f displacement);
	public abstract void setOrientation(float upAngle, float rightAngle);

	public abstract Vector3f up();
	public abstract Vector3f forward();
	public abstract Vector3f right();

}
