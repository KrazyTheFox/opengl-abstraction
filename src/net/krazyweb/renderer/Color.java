package net.krazyweb.renderer;


public class Color {

	public static final Color WHITE = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	public static final Color BLACK = new Color(0.0f, 0.0f, 0.0f, 0.0f);
	public static final Color RED = new Color(1.0f, 0.0f, 0.0f, 1.0f);
	
	public float r, g, b, a;
	
	public Color(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	public Color(int r, int g, int b, int a) {
		this.r = (float) (r) / 255.0f;
		this.g = (float) (g) / 255.0f;
		this.b = (float) (b) / 255.0f;
		this.a = (float) (a) / 255.0f;
	}
	
}