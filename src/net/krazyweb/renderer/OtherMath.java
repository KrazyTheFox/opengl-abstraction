package net.krazyweb.renderer;

public class OtherMath {
	
	public static float clamp(float value, float min, float max) {
		return Math.max(Math.min(value, max), min);
	}
	
}