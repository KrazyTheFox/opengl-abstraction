package net.krazyweb.renderer;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;


public interface Renderer {
	
	public static enum TEXTURE_FORMAT {
		
		PNG("PNG"),
		JPG("JPG"),
		TIF("TIF");
		
		public final String type;
		
		private TEXTURE_FORMAT(String t) {
			type = t;
		}
		
	}
	
	public static enum TEXTURE_HINT {
		NEAREST, BILINEAR;
	}
	
	public static enum TEXTURE_TYPE {
		RGB, RGBA16F, RGBA32F;
	}
	
	public static enum BLEND_MODE {
		ADDITIVE, TRANSPARENCY
	}
	
	public abstract void setClearColor(Color color);
	
	public abstract void createContext(int width, int height);
	
	public abstract int beginMesh();
	public abstract int addVertexToMesh(Vector3f point);
	public abstract int addVertexToMesh(Vector3f point, Color color);
	public abstract int addVertexToMesh(Vector3f point, Vector2f texture);
	public abstract int addVertexToMesh(Vector3f point, Vector3f normal, Color color);
	public abstract int addVertexToMesh(Vector3f point, Vector2f texture, Color color);
	public abstract int addVertexToMesh(Vector3f point, Vector3f normal, Vector2f texture, Color color);
	public abstract void addTriangleToMesh(int vertex1, int vertex2, int vertex3);
	public abstract void finishMesh();
	public abstract void drawMesh(int meshID);
	public abstract void deleteMeshBuffers();
	public abstract void deleteMesh(int meshID);
	
	public abstract void batchMesh(int receivingMesh, int meshToBatch, Vector3f batchTranslation, boolean deleteMeshToBatch);
	
	public abstract int getQuad(float x, float y, float z, float width, float height);
	
	public abstract void setMatrix(Matrix4f matrix);
	
	public abstract int createShader(String vertexShader, String fragmentShader);
	public abstract void useShader(int shader);
	public abstract void setShaderProperty(String property, Color color);
	public abstract void setShaderProperty(String property, int value);
	public abstract void setShaderProperty(String property, float value);
	public abstract void setShaderProperty(String property, Vector2f value);
	
	public abstract int loadTexture(String texture, TEXTURE_FORMAT format, TEXTURE_HINT hint);
	public abstract void bindTexture(int texture, int textureUnit);
	public abstract void unbindTexture(int textureUnit);
	
	public abstract int beginFBO(int width, int height, int numberOfTextures);
	public abstract void addTextureToFBO(TEXTURE_TYPE type);
	public abstract void finishFBO();
	
	public abstract void enableFBO(int fbo);
	public abstract void disableFBO();
	public abstract int getFBOTexture(int fbo, int texture);
	
	public abstract void enableBlending(BLEND_MODE blendMode);
	public abstract void disableBlending();
	
	public abstract void clear();
	public abstract void syncFrames(int frameRate);
	public abstract void update();
	
	public abstract boolean isClosed();
	
	public abstract void destroy();
	
}